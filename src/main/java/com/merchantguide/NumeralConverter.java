
package com.merchantguide;

import com.merchantguide.exceptions.InvalidNumeralException;

/**
 * Interface for numeral conversion
 *
 * @author VTiwari
 *
 * @see 
 *
 * @Date 8-Oct-2015
 *
 */
public interface NumeralConverter 
{
	/**
	 * This method will convert given numeral to its equivalent Arabic numeral
	 * 
	 * @param numeral 
	 * @return Arabic equivalent of numeral
	 * @throws InvalidNumeralException if given numeral is invalid
	 */
	public int convertToArabicNumeral(String numeral) throws InvalidNumeralException;
	
	
	/**
	 * This method will check whether given numeral is valid or not.
	 * 
	 * @param numeral
	 * @return True if numeral is valid numeral.
	 */
	public boolean isValidRomanNumeral(String numeral);
}
