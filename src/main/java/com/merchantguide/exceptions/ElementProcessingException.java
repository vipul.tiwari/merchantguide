
package com.merchantguide.exceptions;

/**
 * Thrown to indicate Element Processing exception.
 *
 * @author vipul
 * @see 
 * @Date 8-Oct-2015
 *
 */
public class ElementProcessingException extends Exception
{
	
	private static final long serialVersionUID = -7317184850233575845L;

	/**
     * Constructs an <code>ElementProcessingException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
	public ElementProcessingException(String msg)
	{
		super(msg);
	}
}
