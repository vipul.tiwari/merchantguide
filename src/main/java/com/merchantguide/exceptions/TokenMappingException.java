
package com.merchantguide.exceptions;

/**
 * Thrown to indicate Token mapping exception.
 *
 * @author vipul
 * @see 
 * @Date 8-Oct-2015
 *
 */
public class TokenMappingException extends Exception 
{
	private static final long serialVersionUID = 4819018588218770986L;
	
    /**
     * Constructs an <code>TokenMappingException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
	public TokenMappingException(String msg)
	{
		super(msg);
	}
	
}
